import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { RegisterComponent } from './register/register.component';

type NewUser = {
  email: string;
  password: string;
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loginForm = this.fb.group({
    email: [null, Validators.compose([Validators.required, Validators.email])],
    password: [null, Validators.required],
  });

  get user() {
    return this.auth.user;
  }

  constructor(
    private fb: FormBuilder,
    private auth: AngularFireAuth,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  onSubmit(): void {
    const credentials: { email: string; password: string } =
      this.loginForm.value;
    this.auth
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        this.snackbar.open('Login successful', 'Close', { duration: 2000 });
      })
      .catch(() => {
        this.snackbar.open(
          'Login not successful. Please enter valid credentials.',
          'Close',
          { duration: 4000 }
        );
      });
  }

  startRegistration(): void {
    const newUser = {
      email: '',
      password: '',
    };
    this.dialog
      .open(RegisterComponent, { width: '80%', data: newUser })
      .afterClosed()
      .subscribe((newUser: NewUser | string) => {
        console.log(newUser);
        if (typeof newUser === 'string') {
          this.snackbar.open('Cancelled Registration!', 'Close', {
            duration: 2500,
          });
          return;
        }
        this.registerUser(newUser);
      });
  }

  private registerUser(newUser: NewUser) {
    if (!newUser.email || !newUser.password)
      throw new Error('Missing values in newUser');
    this.auth
      .createUserWithEmailAndPassword(newUser.email, newUser.password)
      .then((credentials) => {
        console.log(credentials);
        this.snackbar.open('Registration successful');
      })
      .catch(() => {
        this.snackbar.open('Registration not successful');
      });
  }

  logout() {
    this.auth.signOut();
  }
}
