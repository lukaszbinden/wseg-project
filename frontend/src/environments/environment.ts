// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBob5w3POlQbbIkAofQfrCOJIOjV9ElqrI",
    authDomain: "demoprojekt-wseg.firebaseapp.com",
    projectId: "demoprojekt-wseg",
    storageBucket: "demoprojekt-wseg.appspot.com",
    messagingSenderId: "746618426655",
    appId: "1:746618426655:web:ce351ae02868615d0fcc2a",
    measurementId: "G-GQT0CBTTGD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
